# Technical Test

### Installation

Install the dependencies and devDependencies and start the server.

```sh
$ cd **your_directory**
$ npm install
$ composer install
```

Or download entire source code (include dependencies package) here

[Google Drive](https://drive.google.com/file/d/12l3rGzQimmXPBMFEu0Ic9oQsaQnyZPoI/view?usp=sharing)
