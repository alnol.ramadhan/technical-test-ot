<?php

function curl_request($url=""){
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        echo $response;
    }
}

function random_key(){
    $ci =& get_instance();
    $ci->load->helper('string');

    $key = random_string('sha1', 40);

    return $key;
}

function session_get()
{
    $CI =& get_instance();
    $session = ($CI->session->userdata('userid') !== null) ? $CI->session->userdata('userid') : false;
    return $session;
}

function auth_status()
{
    if(!session_get()){
        redirect('register',true);
    }
}