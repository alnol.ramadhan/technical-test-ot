<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_model extends CI_Model {

    public function insert_data($data){
        $this->db->insert('user', $data);
    }

    public function check_available_code($data){
        $result = $this->db->get_where('user', array('activation_code' => $data));
        return $result->row();
    }

    public function set_active($data){
        $this->db->set('is_active', '1');
        $this->db->where('activation_code', $data);
        $this->db->update('user');
    }
}