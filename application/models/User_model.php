<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function get_all_users()
    {
        $query =  $this->db->order_by('created_date','DESC')->get('user');
        return $query->result();
    }
}