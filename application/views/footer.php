    <script>
      const site_url = '<?php echo site_url()?>';
    </script>
    <!-- jQuery -->
    <script src="<?php echo base_url('node_modules/jquery/dist/jquery.min.js')?>"></script>
    <!-- Bootstrap JS -->
    <script src="<?php echo base_url('node_modules/bootstrap/dist/js/bootstrap.min.js')?>"></script>
    <!-- Recapctha -->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <!-- BootstrapValidation -->
    <script src="https://cdn.rawgit.com/PascaleBeier/bootstrap-validate/v2.2.0/dist/bootstrap-validate.js"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url('node_modules/vanilla-datatables/dist/vanilla-dataTables.min.js')?>"></script>
    <!-- Page -->
    <script src="<?php echo base_url('assets/js/main.js')?>"></script>
    <script src="<?php echo base_url('assets/js/validation.js')?>"></script>
  </body>
</html>