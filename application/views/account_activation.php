<div class="container">
  <div class="row">
    <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
      <div class="card card-signin my-5">
        <div class="card-body">
          <h3 class="card-title text-center mb-4">Activation Success</h3>
          <p>Congratulate, your account is active now.</p>
          <p>Please wait and we will redirect you to homepage.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
setTimeout(function () {
    window.location = site_url + 'home'
}, 3000);
</script>