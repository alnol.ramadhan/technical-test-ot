<div class="container-fluid">
  <div class="row no-gutter">
    <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
    <div class="col-md-8 col-lg-6">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row">
            <div class="col-md-9 col-lg-8 mx-auto">
              <h3 class="login-heading mb-4">Register</h3>
              <form id="register-form" role="form" data-toggle="validator">
                <div class="form-label-group">
                  <input type="email" id="email" class="form-control" placeholder="Email address" required autofocus>
                  <label for="email">Email address</label>
                  <div class="invalid-feedback invalid-feedback-email invisible">
                    Please provide a valid email address.
                  </div>
                </div>

                <div class="form-label-group">
                  <input type="text" id="username" class="form-control" placeholder="Your username" required>
                  <label for="username">Username</label>
                </div>

                <div class="form-label-group mx-auto">
                  <div class="g-recaptcha mx-auto" data-sitekey="6Ld59vcUAAAAAA16lp-zVWJF1YZxkt7sqG0_5922" data-callback="success_recaptcha"></div>
                </div>

                <button class="btn btn-lg btn-primary btn-block btn-register text-uppercase font-weight-bold mb-2" type="submit" disabled>Sign up</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>