<div class="container">
  <div class="row">
    <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
      <div class="card card-signin my-5">
        <div class="card-body">
          <h3 class="card-title text-center mb-4">&#127881; Registration Success &#127881;</h3>
          <p>Please check your email and click <strong>"Activate Account"</strong> button to activate your account.</p>
          <p>PS : Check your spam folder if no email on your inbox :)</p>
        </div>
      </div>
    </div>
  </div>
</div>