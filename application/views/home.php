<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
  <div class="container">
    <a class="navbar-brand" href="#">Onetool</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Hi, <?php echo $this->session->userdata('username');?>
        </a>
          <!-- Here's the magic. Add the .animate and .slide-in classes to your .dropdown-menu and you're all set! -->
          <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="<?php echo site_url('home/logout')?>">Logout</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container pb-5">
  <h1 class="mt-5 text-white font-weight-light">User List</h1>
  <table id="table-user" class="table">
    <thead>
      <tr>
        <td>User ID</td>
        <td>Username</td>
        <td>Email</td>
        <td>Status</td>
      </tr>
    </thead>
    <tbody>
      <?php foreach($list as $item){?>
      <tr>
        <td><?php echo $item->uid?></td>
        <td><?php echo $item->username?></td>
        <td><?php echo $item->email?></td>
        <td><?php echo $item->is_active == 1 ? "Active" : "Not Active" ?></td>
      </tr>
      <?php }?>
    </tbody>
</div>

<script>
  const body = document.getElementsByTagName('body')[0];
  body.classList.add("body-home");
</script>