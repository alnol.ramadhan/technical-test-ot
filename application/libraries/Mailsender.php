<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailsender {

    private $ci = "";

    public function __construct(){
        $this->ci =& get_instance();
    }

    public function sendAPI($email="",$mail_template=""){
        $apiUrl = $this->buildUrl($email,$mail_template);
        $request = curl_request($apiUrl);
        
        return $request;
    }

    private function buildUrl($email="",$mail_template=""){
        $host = $this->ci->config->item('api')['elasticmail_base_uri'].$this->ci->config->item('api')['elasticmail_send'];
        $api_key = $this->ci->config->item('api')['elasticmail_api_key'];
        $email_from = $this->ci->config->item('api')['elasticmail_sender'];
        $email_from_name = $this->ci->config->item('api')['elasticmail_sendername'];

        $url = $host . 
                '?apikey=' . $api_key . 
                '&subject=' . urlencode("Please activate your OneTool account") .
                '&from=' . urlencode($email_from) .
                '&fromName=' . urlencode($email_from_name) .
                '&sender=' . urlencode($email_from) .
                '&senderName=' . urlencode($email_from_name) .
                '&to=' . urlencode($email) .
                '&bodyHtml=' . urlencode($mail_template);

        return $url;
    }

}