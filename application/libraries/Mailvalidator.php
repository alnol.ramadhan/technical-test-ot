<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailvalidator {

    private $ci = "";

    public function __construct(){
        $this->ci =& get_instance();
    }

    public function getAPI($email=""){
        $apiUrl = $this->buildUrl($email);
        $request = curl_request($apiUrl);
        
        return $request;
    }

    private function buildUrl($email=""){
        $host = $this->ci->config->item('api')['debounce_base_uri'];
        $api_key = $this->ci->config->item('api')['debounce_api_key'];

        $url = $host . '?api=' . $api_key . '&email=' . $email;

        return $url;
    }

}