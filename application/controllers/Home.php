<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
        parent::__construct();
        auth_status();
		$this->load->model('user_model','user');
    }
    
    /**
     * List of registered user
     */
    public function index(){
        $all_users = $this->user->get_all_users();

        $data['list'] = $all_users;

        $this->load->view('header');
        $this->load->view('home',$data);
        $this->load->view('footer');
    }

    /**
     * Logout and redirect to register page
     */
    public function logout(){
        $this->session->unset_userdata('userid');
        $this->session->unset_userdata('username');
        redirect('register','refresh');
    }
}