<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Ramsey\Uuid\Uuid;

class Register extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('register_model','register');
	}

	/**
	 * Registration form
	 */
	public function index(){
		$this->load->view('header');
		$this->load->view('register');
		$this->load->view('footer');
	}

	/**
	 * Real email validation using Debounce API
	 */
	public function email_validation(){
		$this->load->library('mailvalidator');

		$email = $this->input->post('email',TRUE);

		if($email){
			$is_email_valid = $this->mailvalidator->getAPI($email);

			echo $is_email_valid;
		}else{
			echo "Err : Please input email";
		}
	}

	/**
	 * Add data to database after success input validation
	 */
	public function add_data(){
		$this->load->library('mailsender');

		$email = $this->input->post('email',TRUE);
		$username = $this->input->post('username',TRUE);
		$uid = Uuid::uuid4();
		$randomkey = random_key();

		$data = [
			'uid' => $uid,
			'email' => $email,
			'username' => $username,
			'activation_code' => $randomkey
		];

		$this->register->insert_data($data);

		$mail_template = $this->mail_template($data);
		$this->mailsender->sendAPI($email,$mail_template);
	}

	/**
	 * Landing page after success add_data()
	 */
	public function success(){
		$this->load->view('header');
		$this->load->view('register_success');
		$this->load->view('footer');
	}

	/**
	 * Account activation landing page
	 */
	public function account_activation($activation_code=""){
		if($activation_code <> ""){
			$activation_code = $this->security->xss_clean($activation_code);
			$check_code = $this->register->check_available_code($activation_code);
			
			if(!empty($check_code)){
				// Set session
				$userdata = [
					'userid' => $check_code->uid,
					'username' => $check_code->username
				];
				$this->session->set_userdata($userdata);

				// Change status to active
				$this->register->set_active($activation_code);

				$this->load->view('header');
				$this->load->view('account_activation');
				$this->load->view('footer');
			}else{
				redirect('register');
			}
		}else{
			redirect('register');
		}
	}

	/**
	 * HTML email template
	 */
	public function mail_template($data){
		$mail_template = "<h3>Hi ".$data['username'].",</h3>
		<p>Thank you for joining Onetool!</p>
		<p>To activate your account please click the link below to verify your email address:</p>
		<p><a href='http://localhost/hiring-test/onetool/register/account_activation/".$data['activation_code']."'>http://localhost/hiring-test/onetool/register/account_activation/".$data['activation_code']."</a></p>
		<p>Good luck! Hope it works.</p>";
		
		return $mail_template;
	}
}
