const form_register = document.getElementById("register-form");
if(form_register !== null){
    bootstrapValidate('#email', 'email:Please provide correct email format.|required:Can\'t be empty.|min:4:Enter at least 4 Characters.');
    bootstrapValidate('#username', 'required:Can\'t be empty');
}