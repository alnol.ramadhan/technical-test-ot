$(function(){
  $('.btn-register').on('click',function(e){
    e.preventDefault();
    
    const emailInput = $('#email').val();
    const usernameInput = $('#username').val();

    if(emailInput !== '' && usernameInput !== '' && ($('#register-form').parent().find('.has-error-email').length == 0 || $('#register-form').parent().find('.has-error-email').is(':hidden'))){
      button_spinner();
      real_email_validation(emailInput,usernameInput);  
    }
  });
});

/**
 * Datatables
 */
const table_user = document.getElementById("table-user");
if(table_user !== null){
  var dataTable = new DataTable("#table-user");
}

/**
 * Callback handler from recaptcha
 */
function success_recaptcha(){
  $('.btn-register').prop('disabled',false);
}

/**
 * Email validation - with Debounce API
 */
function real_email_validation(emailInput,usernameInput){
  $.ajax({
    url: site_url + 'register/email_validation',
    method: 'post',
    data: {'email':emailInput}
  })
  .done(function(success) {
    const result = JSON.parse(success);
    if(result.debounce.result === "Safe to Send"){
      add_data(emailInput,usernameInput);
    }else{
      $('#email').addClass('is-invalid');
      $('.invalid-feedback-email').removeClass('invisible');
      button_spinner();
    }
  })
  .fail(function(jqXHR) {
    console.log(jqXHR);
  })
}

function add_data(emailInput,usernameInput){
  $.ajax({
    url: site_url + 'register/add_data',
    method: 'post',
    data: {
      'email':emailInput,
      'username':usernameInput
    }
  })
  .done(function(success) {
    window.location = site_url + 'register/success'
  })
  .fail(function(jqXHR) {
    console.log(jqXHR);return false;
  })
}

function button_spinner(){
  const buttonState = $('.btn-register').html();
  if(buttonState === "Sign up")
    $('.btn-register').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');
  else
    $('.btn-register').html('Sign up');
}